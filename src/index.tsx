import React from 'react';
import ReactDOM from 'react-dom';

import App from './component/App';

const container = document.querySelector('#container');
ReactDOM.render(<App />, container);

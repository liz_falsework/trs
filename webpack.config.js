const path = require('path');

const APP_PATH = path.resolve(__dirname, './src/index.tsx');
const BUILD_PATH = path.resolve(__dirname, './dist');

module.exports = {
    entry: APP_PATH,
    output: {
        path: BUILD_PATH,
        filename: 'tsxtest.js'
    },
    devtool: "eval-source-map",
    module: {
        rules: [{
            test: /\.tsx?$/,
            use: ['ts-loader'],
            exclude: /node_modules/
        },{
            test: /\.scss$/,
            use:[
                'style-loader',
                'css-loader',
                'sass-loader'
            ]
        }]
    },
    resolve: {
        extensions: ['.tsx','.ts','.js']
    }
}